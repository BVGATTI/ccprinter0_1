# 如何启动？
电脑上安装好`flutter`, `git`, `chrome浏览器`
```bash
git clone git@gitee.com:BVGATTI/ccprinter0_1.git
cd ccprinter0_1
flutter pub get
# 必须要加--no-sound-null-safety参数
flutter run --no-sound-null-safety -d chrome
```
# 如何得到最新的代码
```bash
git pull
```

# ccprinter0_1

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
