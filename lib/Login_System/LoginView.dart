import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'LoginModel.dart';
// import 'LoginViewModel.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LoginPicArea loginPicArea = new LoginPicArea(); //登录图片区
    LoginFunArea loginFunArea = new LoginFunArea(); //登录功能区
    LoginPrivacyPolicyArea loginPrivacyPolicyArea =
        new LoginPrivacyPolicyArea(); //登录隐私政策区
    LoginSplitLineArea loginSplitLineArea = new LoginSplitLineArea(); //分割线
    LoginOtherMethodsArea loginOtherMethodsArea =
        new LoginOtherMethodsArea(); //其他登录方式区
    return Scaffold(
      //登陆页面不想要上面的工具栏，或者上面的工具栏需要填充透明色，然后放个帮助什么的
      // appBar: AppBar(
      //   title: Text("login"),
      // ),
      body: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            child: loginPicArea, //登录图片区对象
            flex: 1,
          ),
          //占位置用的空白框
          SizedBox(
            height: 10,
          ),
          loginFunArea, //登录功能区对象
          loginPrivacyPolicyArea, //登录隐私政策区
          loginSplitLineArea, //分割线
          loginOtherMethodsArea, //其他登录方式区
        ],
      ),
    );
  }
}

class LoginPicArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "images/1.png",
      fit: BoxFit.fill,
    );
  }
}

class LoginFunArea extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginFunArea();
}

class _LoginFunArea extends State<LoginFunArea> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(right: 20, left: 20), //缩进
          alignment: Alignment.center,
          child: SizedBox(
            width: double.infinity,
            // height: ScreenUtil().setHeight(70),
            child: ElevatedButton(
              style: ButtonStyle(
                elevation: MaterialStateProperty.all(0),
                backgroundColor: MaterialStateProperty.all(Color(0xffFCB605)),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(35))),
              ),
              onPressed: LoginPageShow,
              child: Text(
                '登      录',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 20, left: 20),
          alignment: Alignment.center,
          child: SizedBox(
            width: double.infinity,
            // height: ScreenUtil().setHeight(70),
            child: ElevatedButton(
              style: ButtonStyle(
                elevation: MaterialStateProperty.all(0),
                backgroundColor: MaterialStateProperty.all(Color(0xffFCB605)),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(35))),
              ),
              onPressed: RegisterPageShow2,
              child: Text(
                '注      册',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          ),
        ),
      ],
    );
  }

//测试用函数
  void onPressed() {}

  void LoginPageShow() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => LoginPage()));
  }

  void RegisterPageShow2() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => RegisterPage()));
  }
}

class LoginPrivacyPolicyArea extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPrivacyPolicyArea();
}

class _LoginPrivacyPolicyArea extends State<LoginPrivacyPolicyArea> {
  bool _vlaue = false;

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.horizontal,
      children: [
        Expanded(
          //占位置用的空白框
          child: SizedBox(),
          flex: 1,
        ),
        Expanded(
          child: Checkbox(
            onChanged: (v) {
              setState(() {
                _vlaue = v!;
              });
            },
            value: _vlaue,
          ),
          flex: 1,
        ),
        Expanded(
          child: Row(
            children: [
              Text("我已阅读并同意"),
              GestureDetector(
                child: Text(
                  "《隐私政策》",
                  style: TextStyle(color: Colors.blue),
                ),
                onTap: PrivacyPolicyShow,
              ),
              Text("和"),
              GestureDetector(
                child: Text(
                  "《服务条款》",
                  style: TextStyle(color: Colors.blue),
                ),
                onTap: TermsOfServiceShow,
              ),
            ],
          ),
          //这里需要給各种隐私政策设监听
          flex: 8,
        ),
        // 占位置用的空白框
        Expanded(
          child: SizedBox(),
          flex: 1,
        ),
      ],
    );
  }

//测试用函数
  void onPressed() {}

  void PrivacyPolicyShow() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => PrivacyPolicy()));
  }

  void TermsOfServiceShow() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => TermsOfService()));
  }
}

class LoginSplitLineArea extends StatefulWidget {
  @override
  State<LoginSplitLineArea> createState() => _LoginSplitLineArea();
}

class _LoginSplitLineArea extends State<LoginSplitLineArea> {
  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.horizontal,
      children: [
        Expanded(
          child: SizedBox(),
          flex: 1,
        ),
        Expanded(
          child: DecoratedBox(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1.0)),
          ),
          flex: 3,
        ),
        Expanded(
          child: Container(
            child: Text("其他登录方式"),
            alignment: Alignment.center,
          ),
          flex: 3,
        ),
        Expanded(
          child: DecoratedBox(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1.0)),
          ),
          flex: 3,
        ),
        Expanded(
          child: SizedBox(),
          flex: 1,
        ),
      ],
    );
  }
}

class LoginOtherMethodsArea extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginOtherMethodsArea();
}

class _LoginOtherMethodsArea extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.horizontal,
      children: [
        Expanded(
          flex: 2,
          child: SizedBox(),
        ),
        Expanded(
          flex: 2,
          child: IconButton(
            onPressed: OhterMethordPageShow_qq,
            icon: Icon(IconData(0xe882, fontFamily: 'LoginIcons'),
                color: Colors.black),
          ),
        ),
        Expanded(
          flex: 2,
          child: IconButton(
            onPressed: OhterMethordPageShow_wechat,
            icon: Icon(IconData(0xf0106, fontFamily: 'LoginIcons'),
                color: Colors.black),
          ),
        ),
        Expanded(
          flex: 2,
          child: SizedBox(),
        ),
      ],
    );
  }

  void onPressed() {}

  void OhterMethordPageShow_qq() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => OhterMethord_qq()));
  }

  void OhterMethordPageShow_wechat() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => OtherMethord_wechat()));
  }
}


class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("注册页面"),
      ),
    );
  }
}

class PrivacyPolicy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("隐私政策页面"),
      ),
    );
  }
}

class TermsOfService extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("服务条款页面"),
      ),
    );
  }
}

class OhterMethord_qq extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("qq登录方式页面"),
      ),
    );
  }
}

class OtherMethord_wechat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("微信登录页面"),
      ),
    );
  }
}