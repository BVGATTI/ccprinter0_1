import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../Global.dart';

class LoginPage extends StatelessWidget {
  GlobalKey<FormState> loginKey = GlobalKey<FormState>();
  String userName = "";
  String password = "";
  bool isShowPassWord = false;
 

  LoginPage({Key? key}) : super(key: key);


  login(BuildContext context) async {
    //读取当前的Form状态
    var loginForm = loginKey.currentState;
    //验证Form表单
    if (loginForm != null) {
      if (loginForm.validate()) {
        loginForm.save();
        print('userName: ' + userName + ' password: ' + password);
        if (userName == "111" && password == "111") {
          final prefs = await SharedPreferences.getInstance();
          final setTokenResult = await prefs.setString('user_token', 'g');
          if (setTokenResult) {
            print("登陆成功");
            Global.tag = "true";
            Navigator.of(context).pushNamedAndRemoveUntil('/', (route) => route == null,);
            // MaterialPageRoute(
            //   builder: (context) => HomePageView(flag: "true",)
            // );
          } else {
            print("设置失败prefs.setString");
          }
        } else {
          print("登陆失败");
          Fluttertoast.showToast(
              msg: "账号密码错误",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
      }
    }
  }

  void showPassWord() {
      isShowPassWord = !isShowPassWord;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Form表单示例',
      home: Scaffold(
        body: Column(
          children: <Widget>[
            Container(
                padding: const EdgeInsets.only(top: 100.0, bottom: 10.0),
                child: const Text(
                  'LOGO',
                  style: TextStyle(
                      color: Color.fromARGB(255, 53, 53, 53),
                      fontSize: 50.0
                  ),
                )
            ),
            Container(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: loginKey,
                child: Column(
                  children: <Widget>[
                    Container(
                      decoration: const BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Color.fromARGB(255, 240, 240, 240),
                                  width: 1.0
                              )
                          )
                      ),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          labelText: '请输入手机号',
                          labelStyle: TextStyle( fontSize: 15.0, color: Color.fromARGB(255, 93, 93, 93)),
                          border: InputBorder.none,
                          // suffixIcon: new IconButton(
                          //   icon: new Icon(
                          //     Icons.close,
                          //     color: Color.fromARGB(255, 126, 126, 126),
                          //   ),
                          //   onPressed: () {

                          //   },
                          // ),
                        ),
                        keyboardType: TextInputType.phone,
                        onSaved: (value) {
                          userName = value!;
                        },
                        validator: (phone) {
                          if(phone == null || phone.isEmpty){
                            return '请输入手机号';
                          }
                          return null;
                        },
                        onFieldSubmitted: (value) {

                        },
                      ),
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Color.fromARGB(255, 240, 240, 240),
                                  width: 1.0
                              )
                          )
                      ),
                      child: TextFormField(
                        decoration:  InputDecoration(
                            labelText: '请输入密码',
                            labelStyle: const TextStyle( fontSize: 15.0, color: Color.fromARGB(255, 93, 93, 93)),
                            border: InputBorder.none,
                            suffixIcon: IconButton(
                              icon: Icon(
                                isShowPassWord ? Icons.visibility : Icons.visibility_off,
                                color: const Color.fromARGB(255, 126, 126, 126),
                              ),
                              onPressed: showPassWord,
                            )
                        ),
                        obscureText: !isShowPassWord,
                        onSaved: (value) {
                          password = value!;
                        },
                        validator: (password) {
                          if(password == null || password.isEmpty){
                            return '请输入密码';
                          }
                          return null;
                        },
                        onFieldSubmitted: (value) {

                        },
                      ),
                    ),

                    Container(
                      height: 45.0,
                      margin: const EdgeInsets.only(top: 40.0),
                      child: SizedBox.expand(
                        child: RaisedButton(
                          onPressed: () => login(context),
                          color: const Color.fromARGB(255, 61, 203, 128),
                          child: const Text(
                            '登录',
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Color.fromARGB(255, 255, 255, 255)
                            ),
                          ),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(45.0)),
                        ),
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.only(top: 30.0),
                      padding: EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const <Widget>[
                          Text(
                            '注册账号',
                            style: TextStyle(
                                fontSize: 13.0,
                                color: Color.fromARGB(255, 53, 53, 53)
                            ),
                          ),

                          Text(
                            '忘记密码？',
                            style: TextStyle(
                                fontSize: 13.0,
                                color: Color.fromARGB(255, 53, 53, 53)
                            ),
                          ),
                        ],
                      ) ,
                    ),

                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}