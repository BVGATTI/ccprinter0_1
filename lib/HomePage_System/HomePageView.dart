import 'package:ccprinter0_1/UserInformaion_System/UserInformationView.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Global.dart';
import '../UserInformaion_System/UserInfomationViewToken.dart';

class HomePageView extends StatefulWidget {
  HomePageView({Key? key}) : super(key: key);

  var allPages = [
    Print(),
    ReadWorld(),
    SmartDrawer(),
  ];

  @override
  State<StatefulWidget> createState() => _HomePageView();
}

class _HomePageView extends State<HomePageView> {
  int currentIndex = 0;
  Future<String> getTok() async {
    final prefs = await SharedPreferences.getInstance();
    final getTokenResult = await prefs.getString('user_token');
    return getTokenResult;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widget.allPages[currentIndex],
      backgroundColor: Colors.green,
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "首页",
            //backgroundColor:Colors.blue
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "文档预览",
            //backgroundColor:Colors.blue
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.flip_outlined),
            label: "我的",
            //backgroundColor:Colors.blue
          ),
        ],
        currentIndex: currentIndex,
        onTap: (index) {
          setState(() {
            print("the index is :$index");
            print("flag: " + Global.tag);
            if (index == 2 && Global.tag == "true") {
              widget.allPages[2] = SmartDrawerToken();
            }
            currentIndex = index;
          });
        },
      ),
    );
  }
}


class Print extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Print();
}
class _Print extends State<Print> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("打印"),
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          actions:
          [
            Container(
              height: 20,
              /*width: 20,*/
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomLeft: Radius.circular(20),),
                border: Border.all(width: 1, color: Colors.black12),
              ),
              child:
              IconButton(icon: Icon(Icons.share), onPressed: () {},),
            ),
            Container(
              height: 20,
              /*width: 20,*/
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(20),
                  bottomRight: Radius.circular(20),),
                border: Border.all(width: 1, color: Colors.black12),
              ),
              child:
              IconButton(icon: Icon(Icons.adjust), onPressed: () {},),
            )
          ],
          leading: IconButton(icon: Icon(Icons.home), onPressed: () {},),
        ),
        body:
        Container(
            height: 165,
            margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
            decoration: BoxDecoration(color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(10))),
            child: Column(
              children: [
                Container(
                  child: Row(
                      children: [
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(Icons.insert_drive_file, size: 50),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(margin: EdgeInsets.all(2.0),
                                child: Text("1.活动记录.doc",)),
                            GestureDetector(
                                child: Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width - 100,
                                  margin: EdgeInsets.all(2.0),
                                  color: Colors.grey,
                                  child: Row(
                                    children: [
                                      Text('黑白;A4;单面;共1张'),
                                      Text('设置>')
                                    ],
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceBetween,
                                  ),
                                ),
                                onTap: () {
                                  print("单击事件 ");
                                }
                            ),
                            Container(
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width - 96,
                              child: Row(
                                children: [
                                  Text('￥0.15/张'),
                                  Row(
                                    children: [
                                      SizedBox(child: IconButton(
                                          onPressed: () {},
                                          icon: Icon(Icons.remove),
                                          padding: EdgeInsets.all(0.0),
                                          iconSize: 20),
                                        width: 30,
                                        height: 30,),
                                      ElevatedButton(onPressed: () {},
                                          child: Text("1",
                                            style: TextStyle(fontSize: 15),),
                                          style: ElevatedButton.styleFrom(
                                              minimumSize: const Size(30, 30))),
                                      SizedBox(child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(Icons.add),
                                        padding: EdgeInsets.all(0.0),
                                        iconSize: 20,), width: 30, height: 30,),
                                    ],
                                  )
                                ],
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                              ),
                            ),
                          ],
                        )
                      ]
                  ),
                ),
                const Divider(
                  height: 1,
                  // Divider 组件高度
                  thickness: 1,
                  // 分割线线宽，分割线居于 Divider 中心
                  indent: 5,
                  // 分割线左侧间距
                  endIndent: 5,
                  // 分割线右侧间距
                  color: Colors.grey, // 分割线颜色
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  width: MediaQuery
                      .of(context)
                      .size
                      .width - 16,
                  child: Row(
                    children: [
                      Text('小计:￥0.15'),
                      Container(
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.all(2.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10))),
                              child: OutlinedButton.icon(onPressed: () {},
                                  icon: Icon(Icons.settings),
                                  label: Text('设置')),
                            ),
                            Container(
                              margin: EdgeInsets.all(2.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10))),
                              child: OutlinedButton.icon(onPressed: () {},
                                  icon: Icon(Icons.preview),
                                  label: Text('预览')),
                            ),
                            Container(
                              margin: EdgeInsets.all(2.0),
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10))),
                              child: OutlinedButton.icon(onPressed: () {},
                                  icon: Icon(Icons.delete),
                                  label: Text(
                                      '删除') /*,style: ButtonStyle(minimumSize: MaterialStateProperty.all(Size(30, 30)))*/),
                            ),
                          ],
                        ),
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),),

              ],
            )
        )
    );
  }
}



class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text(
        "主页",
        style: TextStyle(color: Colors.black, fontSize: 20),
      ),
    );
  }
}

class ReadWorld extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text("文档预览"),
    );
  }
}

class UserInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text("个人中心"),
    );
  }
}