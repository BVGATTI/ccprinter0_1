import 'package:flutter/material.dart';

import 'HomePage_System/HomePageView.dart';
import 'Login_System/LoginModel.dart';
import 'Login_System/LoginView.dart';

//软件启动的入口地址
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CCprinter Demo',
      // home: LoginView(),
      //这里放路由表，就是home 这个和routes 里的 "/"是不兼容的
      routes: {
        '/': (context) => HomePageView(),
        '开始': (context) => LoginView(),
        "登录": (context) => LoginPage(),
        "注册": (context) => RegisterPage(),
        "隐私政策": (context) => PrivacyPolicy(),
        "服务条款": (context) => TermsOfService(),
        "qq登录": (context) => OhterMethord_qq(),
        "微信登录": (context) => OtherMethord_wechat(),
      },
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("共享打印"),
      ),
      body: const Text("这是一款共享打印App"),
    );
  }
}