import 'package:flutter/material.dart';

import '../Global.dart';

class SmartDrawerToken extends StatelessWidget {
  final double elevation = 16.0;
  final Widget child = const UserInformationView2(); //设置展示内容
  final String semanticLabel = "123";
  final double widthPercent = 0.7;

  SmartDrawerToken({Key? key}) : super(key: key);

  // 设置宽度比例
  @override
  Widget build(BuildContext context) {
    ///获取宽度
    final double _width = MediaQuery.of(context).size.width * widthPercent;

    ///new end
    return ConstrainedBox(
      ///edit start
      constraints: BoxConstraints.expand(width: _width),

      ///edit end
      child: Material(
        elevation: elevation,
        child: child,
      ),
    );
  }
}

class UserInformationView extends StatelessWidget {
  const UserInformationView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "images/1.png",
      fit: BoxFit.fill,
    );
  }
}

class UserInformationView2 extends StatelessWidget {
  const UserInformationView2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(padding: EdgeInsets.zero, children: [
      Container(
          decoration: const BoxDecoration(
            color: Colors.blue,
            // image: DecorationImage(
            //
            //   // image:
            // )
          ),
          height: 200,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //圆形组件
                  const CircleAvatar(
                    radius: 30,
                    backgroundImage: NetworkImage(
                        "https://upload.jianshu.io/users/upload_avatars/2268884/df618e28-c6d0-43b6-a7e9-80a7da48d3db.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/300/h/300/format/webp"),
                  ),
                  const SizedBox(
                    width: 40,
                  ),
                  GestureDetector(
                    child: const Text(
                      "欢迎您 \"admin\" 点击退出登陆",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20),
                    ),
                    onTap: () {
                      Global.tag = "false";
                      Navigator.of(context).pushNamedAndRemoveUntil('/', (route) => route == null,);
                    },
                  )
                ],
              )
            ],
          )),
    ]);
  }
}