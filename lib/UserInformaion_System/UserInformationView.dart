import 'package:ccprinter0_1/Login_System/LoginView.dart';
import 'package:flutter/material.dart';

class SmartDrawer extends StatelessWidget {
  final double elevation = 16.0;
  final String semanticLabel = "123";
  final Widget child = UserInformationView2(); //设置展示内容
  final double widthPercent = 0.7;

  @override
  Widget build(BuildContext context) {
    ///获取宽度
    final double _width = MediaQuery.of(context).size.width * widthPercent;

    ///new end
    return ConstrainedBox(
      ///edit start
      constraints: BoxConstraints.expand(width: _width),

      ///edit end
      child: Material(
        elevation: elevation,
        child: child,
      ),
    );
  }
}

class UserInformationView extends StatelessWidget {
  const UserInformationView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "images/1.png",
      fit: BoxFit.fill,
    );
  }
}

class UserInformationView2 extends StatelessWidget {
  const UserInformationView2({Key? key}) : super(key: key);
  final String content = "游客请登录";

  @override
  Widget build(BuildContext context) {
    return ListView(padding: EdgeInsets.zero, children: [
      Container(
          decoration: const BoxDecoration(
            color: Colors.blue,
            // image: DecorationImage(
            //
            //   // image:
            // )
          ),
          height: 200,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //圆形组件
                  const CircleAvatar(
                    radius: 30,
                    backgroundImage: NetworkImage(
                        "https://upload.jianshu.io/users/upload_avatars/2268884/df618e28-c6d0-43b6-a7e9-80a7da48d3db.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/300/h/300/format/webp"),
                  ),
                  const SizedBox(
                    width: 40,
                  ),
                  GestureDetector(
                    child: Text(
                      content,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => LoginView()));
                    },
                  )
                ],
              )
            ],
          )),
      Text("1"),
      Text("2"),
      Text("3"),
      Text("4"),
    ]);
  }
}